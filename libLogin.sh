#!/bin/bash

# Script to automatically renew Southampton Library loans.
# Created on 2/06/2016 		Kostas D.
# 
# Comment the lines below if you want to keep you login information
# inside this file. If you do so, make sure to change permissions for the file
# to be read only by root, i.e.:
# 			sudo chown root:root libLogin.sh
# 			sudo chmod 700 libLogin.sh
# 
# User name and password:
#USERN=
#PASS=

if [ -z ${USERN+x} ];then
	echo "USERName is unset. Plese enter it:"
	read USERN;
fi

if [ -z ${PASS+x} ];then
	echo "PASSword is unset. Plese enter it:"
	read PASS;
fi

cat > libLog.tmp <<'endmsg';
# Command logfile created by Lynx 2.8.8rel.2 (09 Mar 2014)
# Arg0 = lynx
# Arg1 = -cmd_script=libLog.txt
# Arg2 = -cmd_log=play.txt
# Arg3 = https://www-lib.soton.ac.uk/
key ^J
key Down Arrow
key Down Arrow
endmsg
for (( i=0; i<${#USERN}; i++ )); do
  echo -e "key ${USERN:$i:1}" >> libLog.tmp
done
echo -e "key Down Arrow\n" >> libLog.tmp
for (( i=0; i<${#PASS}; i++ )); do
  echo -e "key ${PASS:$i:1}" >> libLog.tmp
done

cat >> libLog.tmp <<'endmsg';
key Down Arrow
key Right Arrow
key Down Arrow
key Down Arrow
key Down Arrow
key Down Arrow
key Down Arrow
key Down Arrow
key Down Arrow
key Right Arrow
key Down Arrow
key Down Arrow
key Down Arrow
key Down Arrow
key Down Arrow
key Down Arrow
key Down Arrow
key Down Arrow
key Down Arrow
key Down Arrow
key Down Arrow
key Down Arrow
key Down Arrow
key Right Arrow
key Down Arrow
key Down Arrow
key Down Arrow
key Down Arrow
key Down Arrow
key Down Arrow
key Down Arrow
key Down Arrow
key Down Arrow
key Down Arrow
key Down Arrow
key ^J
key Down Arrow
key Down Arrow
key Down Arrow
key Right Arrow
key ^ˇ
key P
key ^J
key ^J
key Q
endmsg

lynx -cmd_script=libLog.tmp https://www-lib.soton.ac.uk/;

OUT="$(grep -o "renewed" 93 | wc -l)";
if [[ $OUT -gt 2 ]]; then
	echo 'Success! All items renewed';
fi

rm -f libLog.tmp 93
