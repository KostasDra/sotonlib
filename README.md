# README #

### Automatically renew library loans###
Use this script and the Command Line Browser [lynx](http://lynx.browser.org/) to automatically renew all library loans at the University of Southampton. 

### How do I get set up? ###

Get the lynx web browser either from the [website](http://lynx.browser.org/) or through the application manager:

* OSX 
```
#!bash

brew install lynx
```
* Linux

```
#!bash

sudo apt-get install lynx
```
Go to the folder containing the file and change its permissions to make it executable:
```
#!bash

chmod +x libLogin.sh
```
Run it:
```
#!bash

./libLogin.sh
```
A ```success``` message should appear in the terminal. If it does not exit press ```Q``` and retry with the correct user name and password. If it still doesn't work send me a message.